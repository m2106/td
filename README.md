## Les Travaux dirigés du modules M2106 

Ce dépot vient en complément des documents ou information pouvant se trouver sur 
le moodle du module. En aucun cas il n'est la source officielle d'information pour
le module.

- [TD1](td1.md)
- [TD2](td2.md)