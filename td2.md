__AVANT CHAQUE SEANCE DE TD ASSUREZ-VOUS D'AVOIR RELU VOTRE COURS__


Cette partie du dépot contient votre TD2 (sujet et fichiers).

- Le sujet du TD est [ici](https://gitlab.com/m2106/td/raw/master/files/td2.pdf)
- Le fichier pcap associé du TD est [ici](https://gitlab.com/m2106/td/raw/master/files/TramesHTTP.pcapng)